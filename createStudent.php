<?php
  include('functions.php');

  if(isset($_POST['full_name']) && isset($_POST['email'])&&  isset($_POST['id_carrera'])) {
  
    $detalle=$_POST['id_carrera'];
    $saved = saveStudent($_POST);

    if($saved) {
      header('Location: /dashboard/web/crud/?status=success');
    } else {
      header('Location: /dashboard/web/crud/?status=error');
    }
  } else {
    header('Location: /dashboard/web/crud/?status=error');
  }
