<?php
include('functions.php');

$id = $_GET['id'];
if($id) {
  $student = getStudent($id);
  if($student) {
    $deleted = deleteStudent($id);
    if($deleted) {
      header('Location:  /dashboard/web/crud/?status=success');
    } else {
      header('Location: /dashboard/web/crud/?status=error');
    }
  } else {
    header('Location:  /dashboard/web/crud/?status=error');
  }
} else {
  header('Location: /index.php');
}